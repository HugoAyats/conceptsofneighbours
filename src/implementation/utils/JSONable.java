package implementation.utils;

public abstract class JSONable {
    public abstract String toJson();
}
